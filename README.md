# hissyblog-freebsd-verify-clojure

Simple Clojure toolchain verification by way of hello world.

This forms part of a series of blog posts on [Hissyblog](https://hissyfit.gitlab.io/hissyblog/) regarding the use of FreeBSD for software development.

## Prerequisites

An installation of [Clojure](https://clojure.org) and [Leiningen](https://leiningen.org).

## Building and Running

```shell
lein run
```

## Copyright and License

Copyright © 2023 Kevin Poalses

Distributed under the [Simplified BSD License](./LICENSE.md).
