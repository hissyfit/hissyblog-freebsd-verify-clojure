(defproject hissyblog-freebsd-verify-clojure "0.1.0-SNAPSHOT"
  :description "Simple Clojure toolchain verification by way of hello world"
  :url "https://hissyfit.gitlab.io/hissyblog/"
  :license {:name "Simplified BSD License"
            :url "https://opensource.org/license/bsd-2-clause/"}
  :dependencies [[org.clojure/clojure "1.11.1"]]
  :main ^:skip-aot hissyblog-freebsd-verify-clojure.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
